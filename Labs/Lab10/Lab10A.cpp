  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 10
*/

#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
using namespace std;

void voweldeath(string sad){
  int numvow = 0;
  for (size_t i = 0; i < (sad.length()); i++) {
    if ((sad[i] == 'a')||(sad[i] == 'e')||(sad[i] == 'i')||(sad[i] == 'o')||(sad[i] == 'u')) {
      sad[i] = '*';
      numvow++;
    }
  }
  cout << sad;
  cout << "\nThat string has " << numvow << " vowels.";
}

int main(){
  string stri;
  cout << "Enter a string: ";
  getline(cin, stri);
  voweldeath(stri);
}
