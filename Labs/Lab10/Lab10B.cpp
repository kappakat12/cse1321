  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 10
*/

#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
using namespace std;

string dang(string darn){
  int i = 0;

  while (darn.find("dang", i) != string::npos) {
    int f = darn.find("dang", i);
    cout << "\nFound bad word at: " << f;
    darn[f] = '&';
    darn[f+1] = '^';
    darn[f+2] = '#';
    darn[f+3] = '@';
    i++;
  }
  cout << "\n";
  return(darn);
}

int main(){
  string stri;
  cout << "Enter a string: ";
  getline(cin, stri);
  cout << dang(stri);
}
