  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 10
*/

#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
using namespace std;

int main(){
  string s;
  cout << "Enter a string: ";
  getline(cin, s);

  for (size_t i = 0; i < s.length(); i++) {
    if (s[i] > 96) {
      s[i] -= 97;
      s[i] += 13;
      s[i] = s[i] % 26;
      s[i] += 97;
    }
  }
  cout << s;
}
