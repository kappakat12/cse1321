  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 3
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int quarters, dimes, nickels, pennies;
  float dollars;
  cout << "Enter the number of quarters: ";
  cin >> quarters;
  cout << "Enter the number of dimes: ";
  cin >> dimes;
  cout << "Enter the number of nickels: ";
  cin >> nickels;
  cout << "Enter the number of pennies: ";
  cin >> pennies;
  dollars = (quarters * .25) + (dimes * .10) + (nickels * .05) + (pennies * .01);
  cout << "\nYou entered " << quarters << " quarters.\n";
  cout << "You entered " << dimes << " dimes.\n";
  cout << "You entered " << nickels << " nickels.\n";
  cout << "You entered " << pennies << " pennies.\n\n";
  int dollar = (quarters * 25) + (dimes * 10) + (nickels * 05) + (pennies * 01);
  cout << "Your total is " << floor(dollars) << " dollars and " << dollar % 100 << " cents.";

}
