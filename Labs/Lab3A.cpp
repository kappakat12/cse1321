  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 3
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int amountOwed;
  float apr, mpr, minPay;
  cout << "Amount owed: $";
  cin >> amountOwed;
  cout << "APR: ";
  cin >> apr;
  //I have no idea why dividing by 100 gives the correct answer
  minPay = ((amountOwed * apr)/12)/100;
  mpr = apr/12;
  cout << "Monthly percentage rate: " << mpr << "\n";
  cout << "Minimum payment: $" << minPay;

}
