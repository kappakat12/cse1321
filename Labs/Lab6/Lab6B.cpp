  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 6
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int min, max, input;
  cout << "Enter a number between -1000 and +1000: ";
  cin >> input;
  max = input;
  min = input;
  cout << "Min is " << min;
  cout << "\nMax is " << max;
  while (input != 0) {
    cout << "\nEnter a number between -1000 and +1000: ";
    cin >> input;
    if (input > max) {
      max = input;
    }else if(input < min){
      min = input;
    }
    cout << "Min is " << min;
    cout << "\nMax is " << max;
  }
}
