  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 6
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  string cookie;
  do {
    cout << "Gimme a cookie: ";
    getline(cin, cookie);
  } while(cookie != "cookie");
}
