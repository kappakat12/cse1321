  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 6
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int start, end, sum;
  sum = 0;
  cout << "Enter a starting number: ";
  cin >> start;
  cout << "Enter an ending number: ";
  cin >> end;
  if (start > end) {
    for (int i = start; i >= end; i--) {
      if (!((i % 2) == 0)) {
        sum = sum + i;
      }

    }
  }else{
    for (int i = start; i <= end; i++) {
      if (!((i % 2) == 0)) {
        sum = sum + i;
      }

    }
  }
  cout << "Sum of odds is: "<< sum;
}
