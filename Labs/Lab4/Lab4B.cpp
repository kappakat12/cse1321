  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 4
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int numHours, earnings;
  cout << "Number of hours this week: ";
  cin >> numHours;

  if (numHours > 40) {
    earnings = (40 * (15)) + ((numHours - 40) * 25);
  }else{
    earnings = numHours * 15;
  }

  cout << "Earnings: $" << earnings;
}
