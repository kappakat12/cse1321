  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 4
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  string day;
  cout << "Enter the day: ";
  cin >> day;
  if (day == "Monday") {
    cout << "Sounds like someone has a case of the Mondays!";
  }else if (day == "Wednesday") {
    cout << "It's hump day! El ombligo!";
  }else if (day == "Friday") {
    cout << "Finally. It's Friday!";
  }else{
    cout << "It's another day of the week";
  }
}
