  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 7
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  bool err = 0;
  int sizeofarray;
  cout << "Enter the size of the arrays: ";
  cin >> sizeofarray;
  int temp, tempB;
  for (int i = 0; i < sizeofarray; i++) {
    cout << "Enter array 1 slot " << i << ": ";
    cin >> temp;
    cout << "Enter array 2 slot " << i << ": ";
    cin >> tempB;
    if(temp != tempB){
      err = 1;
    }
  }
  if(err){
    cout << "The arrays are not identical";
  }else{
    cout << "The arrays are identical";
  }
}
