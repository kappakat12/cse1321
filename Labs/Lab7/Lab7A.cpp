  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 7
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int sizeofarray;
  cout << "Enter the size of the array:";
  cin >> sizeofarray;
  int array[sizeofarray];
  for (int p = 0; p < sizeofarray; p++) {
    array[p] = (p*p);
    cout << "| " << array[p] << " ";
  }
}
