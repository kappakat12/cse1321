  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  float width, length, sfPerDot;

  cout << "Width: \n";
  cin >> width;
  cout << "Length: \n";
  cin >> length;
  cout << "Square feet per dot: \n";
  cin >> sfPerDot;

  int sqft = width * length;
  float dots = sqft / sfPerDot;

  cout << "A yard of " << sqft << " square feet will take " << dots << " dots to cut.";

}
