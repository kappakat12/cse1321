  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 9
*/

#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
using namespace std;


int max(int A, int B, int C){
  int arr[3] = {A, B, C};
  int holder;
  for(int i = (sizeof(arr)/sizeof(arr[0]))-1; i > 0; i--){
    for (int j = 0; j < i; j++) {
      if (arr[i] < arr[j]) {
        holder = arr[j];
        arr[j]=arr[i];
        arr[i]= holder;
      }
    }
  }
  cout << "\nMax is " << arr[2] <<"\n";
  return(0);
}
int min(int A, int B, int C){
  int arr[3] = {A, B, C};
  int holder;
  for(int i = (sizeof(arr)/sizeof(arr[0]))-1; i > 0; i--){
    for (int j = 0; j < i; j++) {
      if (arr[i] < arr[j]) {
        holder = arr[j];
        arr[j]=arr[i];
        arr[i]= holder;
      }
    }
  }
  cout << "Min is " << arr[0] <<"\n";
  return(0);
}
float average(float A, float B, float C){
  float av= (A+B+C)/3;
  cout << "Average is " << av <<"\n";
  return(0);
}


int main(){
  int numA, numB, numC;
  cout << "Enter number 1: ";
  cin >> numA;
  cout << "Enter number 2: ";
  cin >> numB;
  cout << "Enter number 3: ";
  cin >> numC;
  min(numA, numB, numC);
  max(numA, numB, numC);
  average(numA, numB, numC);
}
