  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 9
*/

#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
using namespace std;


void intArray(int a[], int size){
  for (size_t i = 0; i < (size); i++) {
    a[i]=0;
  }
}
void printSum(int a[], int size){
  int sum = 0;
  for (size_t i = 0; i < size; i++) {
    sum += a[i];
  }
  cout << sum << "\n";
}
void enterNum(int a[]){
  int slot;
  cout << "Enter the slot: ";
  cin >> slot;
  cout << "Enter the new value: ";
  cin >> a[slot];
}
void printMenu(){
  cout << "Would you like to:\n1) Enter a number\n2) Print the array\n3) Find the sum of the array\n4) Reset the array\n5) Quit\n";
}


int main(){
  int arr[10];
  intArray(arr, 10);
  bool u = 1;
  int ans;
  while (u) {
    printMenu();
    cin >> ans;
    switch (ans) {
      case 1: enterNum(arr);
              break;
      case 2:
              for (size_t i = 0; i < 10; i++) {
                cout << "|";
                cout << arr[i];
                if (i != 9) {
                }
              }
              cout << "\n";
              break;
      case 3: printSum(arr, 10);
              break;
      case 4: intArray(arr, 10);
              break;
      case 5: u = 0;
              break;

    }
  }






  // cout << "\n";
  // for (size_t i = 0; i < 10; i++) {
  //   cout << arr[i] << "|";
  // }
}
