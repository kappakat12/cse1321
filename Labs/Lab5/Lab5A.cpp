  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 5
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  cout << "Experiencing severe symptoms (Y/N)? ";
  string answer;
  cin >> answer;
  if (answer == "Y") {
    cout << "Seek emergency care.";
  }else{
    cout << "Close contact with someone who has COVID (Y/N)? ";
    cin >> answer;
    if (answer == "Y") {
      cout << "Quarantine and get tested if you feel sick.";
    }else{
      cout << "If you experience other symptoms, isolate and get tested.";
    }
  }
}
