  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 5
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int x, y;
  cout << "Enter x: ";
  cin >> x;
  cout << "Enter y: ";
  cin >> y;
  if ((x == 0) || (y == 0)) {
    // something is on an axis
    if (x == 0 && !(y == 0)) {
      cout << "This point is on the y axis.";
    }else if(!(x == 0) && (y == 0)){
      cout <<"This point is on the x axis.";
    }else if((x == 0) && (y == 0)){
      cout << "This point is the origin.";
    }
  }else{
    if ((x > 0) && (y > 0)) {
      cout << "This point is in the first quadrant.";
    }else if((x > 0) && (y < 0)){
      cout << "This point is in the fourth quadrant.";
    }else if((x < 0) && (y < 0)){
      cout << "This point is in the third quadrant.";
    }else if((x < 0) && (y > 0)){
      cout << "This point is in the second quadrant.";
    }
  }
}
