  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 5
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

string calcDay(int CurrentDay, int DaysTill){
  string returnString;
  int temp = CurrentDay + (DaysTill % 7);
  if (temp >= 7) {
    temp = temp % 7;
  }
  switch (temp) {
    case 0: returnString = "Sunday";
      break;
    case 1:  returnString = "Monday";
      break;
    case 2:  returnString = "Tuesday";
      break;
    case 3:  returnString = "Wednesday";
      break;
    case 4:  returnString = "Thursday";
      break;
    case 5:  returnString = "Friday";
      break;
    case 6:  returnString = "Saturday";
      break;
  }
  return(returnString);
}

int main(){
  cout << "Enter a number that represents today (Sunday==0, Monday==1,...) ";
  int day;
  cin >> day;
  cout << "Enter the number of days until the meeting: ";
  int numDays;
  cin >> numDays;

  cout << "Meeting day is " << calcDay(day, numDays);
}
