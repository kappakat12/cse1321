  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 8
*/

#include <iostream>;
#include <string>;
#include <cmath>;
#include <fstream>;
using namespace std;

int main(){
  int target;
  int arr[15];
  cout << "Enter a target: ";
  cin >> target;
  fstream file;
  file.open("input.txt", fstream::in);
    for (int i = 0; i < 15; i++) {
      file >> arr[i];
      cout << arr[i] << "|";
    }
  file.close();
  cout << "\n";
  // Linear
  for (int i = 0; i < 15; i++) {
    cout << i << " ";
    if (arr[i] == target) {
      break;
    }
  }
  cout <<"\n";
  // Binary
  int mid, first, last;
  first = 0;
  last = 15-1;
  mid = (first+last)/2;
  while (first <= last) {
    cout << mid <<" ";
    if (arr[mid] < target) {
      first = mid+1;
    }else if (arr[mid] == target) {
      break;
    }else{
      last=mid-1;
    }
    mid = (first+last)/2;
  }
}
