  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 8
*/

#include <iostream>;
#include <string>;
#include <cmath>;
#include <fstream>;
using namespace std;

int main(){
  int holder;
  int arr[10];
  int numswap = 0;
  for (int i = 0; i < 10; i++) {
    cout << "Enter slot " << i << ": ";
    cin >> arr[i];
  }
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 9; j++) {
      if (arr[j] > arr[j+1]) {
        holder = arr[j];
        arr[j] = arr[j+1];
        arr[j+1]=holder;
        numswap++;
      }
    }
    for (int p = 0; p < 10; p++) {
      cout << arr[p] << "|";
    }
    cout << " Num swaps: " << numswap << endl;
  }
  ofstream file;
  file.open("outpuut.csv");
  for (int p = 0; p < 10; p++) {
    file << arr[p];
    if (p != 9) {
      file << ",";
    }
  }
  file.close();
}
