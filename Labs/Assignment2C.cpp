  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  double hor, vert, diag;
  cout << "Horizontal pixels: \n";
  cin >> hor;
  cout << "Vertical pixels: \n";
  cin >> vert;
  cout << "Diagonal length in inches: \n";
  cin >> diag;
  cout << "Pixels per inch: " << int(sqrt(pow(vert, 2) + pow(hor, 2)) / diag);
}
