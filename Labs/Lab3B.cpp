  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 3
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  float hoursA, hoursB, hoursC, hoursD;
  float gradeA, gradeB, gradeC, gradeD;

  cout << "Course 1 hours: ";
  cin >> hoursA;
  cout << "Grade for course 1: ";
  cin >> gradeA;
  cout << "Course 2 hours: ";
  cin >> hoursB;
  cout << "Grade for course 2: ";
  cin >> gradeB;
  cout << "Course 3 hours: ";
  cin >> hoursC;
  cout << "Grade for course 3: ";
  cin >> gradeC;
  cout << "Course 4 hours: ";
  cin >> hoursD;
  cout << "Grade for course 4: ";
  cin >> gradeD;
  cout << "Total hours is : " << (hoursA + hoursB +hoursC + hoursD) << "\n";
  cout << "Total quality points is: " << (gradeA*hoursA + gradeB*hoursB + gradeC*hoursC + gradeD*hoursD) << "\n";
  cout << "Your GPA for this semester is " << (gradeA*hoursA + gradeB*hoursB + gradeC*hoursC + gradeD*hoursD) / (hoursA + hoursB +hoursC + hoursD);

}
