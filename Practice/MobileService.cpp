#include <iostream>;
using namespace std;

int main(){
  while (1) {
    string task;
    cout << "\nwhat would you like to do? (q for a quote, n for end): ";
    cin >> task;
    if (task == "q") {

    }else{

      return(0);
    }
    int numMessages;
    cout << "Enter the number of messages used per month: ";
    cin >> numMessages;
    const float A = 15.95l;
    const float B = 25.95;
    double priceA;
    double priceB;

    //C is a constant price
    const float priceC = 45.95;

    //Calculate price using plan A
    if (numMessages > 10){
      priceA = A + ((numMessages-10)*.2);
    }else{
      priceA = A;
    }

    //Calculate price using plan B
    if (numMessages > 20) {
      priceB = B + ((numMessages-20)*.1);
    }else{
      priceB = B;
    }

    double priceDifOne;
    double priceDifTwo;


    if (priceA < priceB && priceA < priceC) {
      cout << "Plan A is the cheapest plan";
      priceDifOne = priceB-priceA;
      priceDifTwo = priceC-priceA;
    }else if(priceB < priceA && priceB < priceC){
      cout << "Plan B is the cheapest plan";
      priceDifOne = priceA-priceB;
      priceDifTwo = priceC-priceB;
    }else if (priceC < priceA && priceC < priceB){
      cout << "Plan C is the cheapest plan";
      priceDifOne=priceA-priceC;
      priceDifTwo=priceB-priceC;
    }

    if (priceDifOne < priceDifTwo) {
      cout << ", you are saving $" << priceDifOne << " compared to the next cheapest plan";
    }else{
      cout << ", you are saving $" << priceDifTwo << " compared to the next cheapest plan";
    }

    //Debug info about prices
    cout << "\n" << priceA << "\n";
    cout << priceB << "\n";
    cout << priceC;
  }

}
