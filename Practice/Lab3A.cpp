  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Lab#: 3
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int amountOwed
  float apr, mpr, minPay;
  cout << "Amount owed: $";
  cin >> amountOwed;
  cout << "APR: ";
  cin >> apr;
  minPay = amountOwed * (apr/12);
  mpr = apr/12;
  cout << "Monthly percentage rate: " << mpr;
  cout << "Minimum payment: $" << minPay;

}
