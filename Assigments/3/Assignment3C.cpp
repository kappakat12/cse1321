  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 3
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  float numPeople, total, addition;
  string answer;
  cout <<  "How many people in your party? ";
  cin >>  numPeople;
  cout <<  "What is the total cost of your bill? ";
  cin >>  total;
  if (numPeople >= 6) {
    cout <<  "Your bill is $" << (total + (total * .18));
    cout <<  "\nWould you like to include an additional tip (Y/N)? ";
    cin >>  answer;
    if (answer == "Y") {
      cout <<  "How much? ";
      cin >>  addition;
      cout <<  "Total bill is: $" << (total + (total * .18) + addition);
    }else{
      cout <<  "Total bill is: $" << (total + (total * .18));
    }
  }else{
    cout <<  "Your bill is $" << total;
    cout <<  "\nHow much for the tip? ";
    cin >>  addition;
    cout <<  "Total bill is: $" << (total + addition);
  }
}
