  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 3
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int numA, numB;
  char opperator;
  cout << "Enter the first number: ";
  cin >> numA;
  cout << "Enter the second number: ";
  cin >> numB;
  cout << "Enter the operator to apply: ";
  cin >> opperator;
  switch (opperator) {
    case '*': cout << (numA * numB);
  break;
    case '/': cout << (numA / numB);
  break;
    case '+': cout << (numA + numB);
  break;
    case '-': cout << (numA - numB);
  break;
  }
}
