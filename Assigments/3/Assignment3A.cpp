  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 3
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  string nameA, nameB;
  cout << "Enter guest 1: ";
  cin >> nameA;
  cout << "Enter guest 2: ";
  cin >> nameB;
  if (((nameA == "Taylor") || (nameA == "Fernando")) && ((nameB == "Fernando") || (nameB == "Taylor"))) {
    cout << "Your party is ruined and another bad pop song will be written.";
  }else{
    cout << "Your party was a hit!";
  }
}
