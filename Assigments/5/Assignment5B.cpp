  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 5
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int numyears, startyear;
  cout << "Enter the number of years: ";
  cin >> numyears;
  cout << "Enter the starting year: ";
  cin >> startyear;
  int arr[numyears];
  int arrB[numyears];
  for (int i = 0; i < numyears; i++) {
    cout << "Enter stat for year " << i+startyear << ": ";
    cin >> arr[i];
  }
  for (int i = 0; i < numyears; i++) {
    arrB[i] = i + startyear;
  }
int holder;
for(int i = (sizeof(arr)/sizeof(arr[0]))-1; i > 0; i--){
  for (int j = 0; j < i; j++) {
    if (arr[i] < arr[j]) {
      holder = arr[j];
      arr[j]=arr[i];
      arr[i]= holder;
      holder = arrB[j];
      arrB[j]=arrB[i];
      arrB[i]= holder;
    }
  }
}
  for (int i = 0; i < numyears; i++) {
    cout << arrB[i] << "|";
  }
}
