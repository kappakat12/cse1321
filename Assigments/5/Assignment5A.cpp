  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 5
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int numyears, startyear;
  cout << "Enter the number of years: ";
  cin >> numyears;
  cout << "Enter the starting year: ";
  cin >> startyear;
  int arr[numyears];
  for (int i = 0; i < numyears; i++) {
    cout << "Enter stat for year " << i+startyear << ": ";
    cin >> arr[i];
  }

  int max, maxyear, min, minyear;
  max = arr[0];
  maxyear = startyear;
  min = arr[0];
  minyear = startyear;
  for (int i = 0; i < (sizeof(arr)/sizeof(arr[0])); i++) {
    if (arr[i] > max) {
      max = arr[i];
      maxyear = i + startyear;
    }
    if (arr[i] < min) {
      min = arr[i];
      minyear = i + startyear;
    }
  }
  cout << "Best stat was " << max << " in year " << maxyear << endl;
  cout << "Worst stat was " << min << " in year " << minyear;
}
