  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 6
*/



#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
using namespace std;

string convert(char c, string st, int i){
  string s;
  switch (c) {
    case 'a': s = "4";
    break;
    case 'b': s = "B";
    break;
    case 'c': s = "(";
    break;
    case 'd': s = "D";
    break;
    case 'e': s = "3";
    break;
    case 'f': s = "Ph";
    break;
    case 'g': s = "9";
    break;
    case 'h': s = "|-|";
    break;
    case 'i': s= "1";
    break;
    case 'j': s = "j";
    break;
    case 'k': s = "|<";
    break;
    case 'l': s = "L";
    break;
    case 'm': s="/\\\\/\\\\";
    break;
    case 'n': s= "|\\\\|";
    break;
    case 'o': s = "0";
    break;
    case 'p': s = "P";
    break;
    case 'q': s = "Q";
    break;
    case 'r': s = "R";
    break;
    case 's': s = "$";
    break;
    case 't': s = "7";
    break;
    case 'u': s= "U";
    break;
    case 'v': s= "\\V";
    break;
    case 'w': s = "\\V\\V";
    break;
    case 'x': s = "><";
    break;
    case 'y': s= "'/";
    break;
    case 'z': s="Z";
    break;
    case ' ': s=" ";
    break;
  }
  st.erase(i);
  st = st.insert(i, s);


  return(s);
}

int main(){
  string s, st;
  cout << "Enter a string: ";
  getline(cin, s);

  for (size_t i = 0; i < s.length(); i++) {
    /* code */
    st += convert(s[i],s,i);
  }
cout << st;
}
