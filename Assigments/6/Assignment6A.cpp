  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 6
*/

#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
using namespace std;

namespace nope {
  float map(float in, float min, float max, float newmin, float newmax){
    return(((in-min)*(newmax-newmin))/(max-min)+newmin);
  }
}

void convert(int red, int blue, int green) {
  cout << "New color is red=" <<   nope::map(red, 0, 255, 0, 1) << ", green=" <<  nope::map(green, 0, 255, 0, 1) << ", blue=" <<nope::map(blue, 0, 255, 0, 1);
}

int main(){
  int red, green, blue;
  cout << "Enter a red value (0-255): ";
  cin >> red;
  cout << "Enter a green value (0-255): ";
  cin >> green;
  cout << "Enter a blue value (0-255): ";
  cin >> blue;
  convert(red, blue, green);
}
