  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 6
*/

#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
using namespace std;

string season(int month, int day){
  int daysInMonth[12] = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30};
  int dayspast=0;
  for (int i = 1; i < month; i++) {
    dayspast+=daysInMonth[i];
  }
  dayspast+= day;
  if ((79 <= dayspast)&&(dayspast<=171)) {
    return("Spring");
  }
  if ((172 <= dayspast)&&(dayspast<=265)) {
    return("Summer");
  }
  if ((266 <= dayspast)&&(dayspast<=355)) {
    return("Fall");
  }
  if ((356 <= dayspast)&&(dayspast<=366)) {
    return("Winter");
  }
  if ((0 <= dayspast)&&(dayspast<=78)) {
    return("Winter");
  }
}

int main(){
  int month, day;
  cout << "Enter a month: ";
  cin >> month;
  cout << "Enter a day: ";
  cin >> day;
  cout << "It is " << season(month, day) << "!";
}
