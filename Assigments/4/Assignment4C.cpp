  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 4
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int width;
  cout << "Width: \n";
  cin >> width;
  for (int i = 0; i < width; i++) {
    if ((i == 0) || (i == (width-1))) {
      for (int j = 0; j < width; j++) {
        cout << "* ";
      }
    }else{
      cout << "* ";
      for (int t = 0; t < (width - 2); t++) {
        cout << "  ";
      }
      cout << "* ";
    }
    if (i != (width-1)){
      cout << "\n";
    }
  }
}
