  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 4
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int seedA, seedB, iter;
  cout << "Enter seed 1: \n";
  cin >> seedA;
  cout << "Enter seed 2: \n";
  cin >> seedB;
  cout << "Number of iterations: \n";
  cin >> iter;
  cout << seedA << "," << seedB << ",";
  int nextval;
  for (int i = 0; i < iter; i++) {
    nextval = seedA + seedB;
    seedA = seedB;
    seedB = nextval;
    if (i != (iter - 1)) {
      cout << nextval << ",";
      /* code */
    }else{
      cout << nextval;
    }
  }
}
