/*
Class: CSE 1321L
Section: 16
Term: Fall 2020
Instructor: Vishal Bilagi
Name: Glen Lewis
Assignment#: 4C
*/

BEGIN MAIN
CREATE width;
PRINTLINE "Width:"
READ width;
FOR i = 0 TO width BY 1
  IF ((i == 0) || (i == (width-1))) THEN
    FOR j = 0 TO width BY 1
      PRINT "* "
    END FOR
  ELSE
    PRINT "* "
    FOR t = 0 TO (width-2) BY 1
      PRINT " " + " "
    END FOR
    PRINT "* "
  END IF
  PRINT "\n"
END FOR
END MAIN
