  /*
  Class: CSE 1321L
  Section: 16
  Term: Fall 2020
  Instructor: Vishal Bilagi
  Name: Glen Lewis
  Assignment#: 4
*/

#include <iostream>;
#include <string>;
#include <cmath>;
using namespace std;

int main(){
  int size;
  cout << "Size: \n";
  cin >> size;
  for (int i = 0; i < size; i++) {
    for (int t = 0; t < i; t++) {
      cout << "  ";
    }
    for (int j = 0; j < (size - i); j++) {
      cout << j << " ";
    }
 cout << endl;
  }
}
